#include "qte.h"
#include "implicits.h"
#include "ui_interface.h"
#include "../node.h"
#include "../signed.h"
#include <iostream>
#include <chrono>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <random>

using namespace std;

const unsigned int EROSION_TYPE = 1;

MainWindow::MainWindow() : QMainWindow(), uiw(new Ui::Assets)
{
	// Chargement de l'interface
    uiw->setupUi(this);

	// Chargement du GLWidget
	meshWidget = new MeshWidget;
	QGridLayout* GLlayout = new QGridLayout;
	GLlayout->addWidget(meshWidget, 0, 0);
	GLlayout->setContentsMargins(0, 0, 0, 0);
    uiw->widget_GL->setLayout(GLlayout);

	// Creation des connect
	CreateActions();

	meshWidget->SetCamera(Camera(Vector(10, 0, 0), Vector(0.0, 0.0, 0.0)));
}

MainWindow::~MainWindow()
{
	delete meshWidget;
}

void MainWindow::CreateActions()
{
	// Buttons
    connect(uiw->boxMesh, SIGNAL(clicked()), this, SLOT(BoxMeshExample()));
	connect(uiw->sphere, &QPushButton::clicked, [=]() {
		OperationExample(SPHERE);
		});
	connect(uiw->cube, &QPushButton::clicked, [=]() {
		OperationExample(CUBE);
		});
	connect(uiw->cone, &QPushButton::clicked, [=]() {
		OperationExample(CONE);
		});
	connect(uiw->tore, &QPushButton::clicked, [=]() {
		OperationExample(TORE);
		});
	connect(uiw->capsule, &QPushButton::clicked, [=]() {
		OperationExample(CAPSULE);
		});
	connect(uiw->cylinder, &QPushButton::clicked, [=]() {
		OperationExample(CYLINDER);
		});

	connect(uiw->translation, &QPushButton::clicked, [=]() {
		OperationExample(TRANSLATION);
		});
	connect(uiw->rotation, &QPushButton::clicked, [=]() {
		OperationExample(ROTATION);
		});
	connect(uiw->scale, &QPushButton::clicked, [=]() {
		OperationExample(SCALE);
		});

	connect(uiw->unioon, &QPushButton::clicked, [=]() {
		OperationExample(UNION);
		});
	connect(uiw->intersection, &QPushButton::clicked, [=]() {
		OperationExample(INTERSECTION);
		});
	connect(uiw->difference, &QPushButton::clicked, [=]() {
		OperationExample(DIFF);
		});
	connect(uiw->union_smooth, &QPushButton::clicked, [=]() {
		OperationExample(UNION_SMOOTH);
		});
	connect(uiw->intersection_smooth, &QPushButton::clicked, [=]() {
		OperationExample(INTERSECTION_SMOOTH);
		});
	connect(uiw->difference_smooth, &QPushButton::clicked, [=]() {
		OperationExample(DIFF_SMOOTH);
		});
	
	connect(uiw->toresComposition, SIGNAL(clicked()), this, SLOT(toresComposition()));
	connect(uiw->lionComposition, SIGNAL(clicked()), this, SLOT(lionComposition()));
	connect(uiw->performances, SIGNAL(clicked()), this, SLOT(measureErosionTime()));
	connect(uiw->box, SIGNAL(clicked()), this, SLOT(measureBoxTime()));
	connect(uiw->resetcameraButton, SIGNAL(clicked()), this, SLOT(ResetCamera()));
    connect(uiw->wireframe, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_1, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_2, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));

	// Widget edition
	connect(meshWidget, SIGNAL(_signalEditSceneLeft(const Ray&)), this, SLOT(editingSceneLeft(const Ray&)));
	connect(meshWidget, SIGNAL(_signalEditSceneRight(const Ray&)), this, SLOT(editingSceneRight(const Ray&)));

	connect(meshWidget, SIGNAL(_signalErosion(const Ray&)), this, SLOT(editingErosion(const Ray&)));
}

void MainWindow::editingSceneLeft(const Ray&)
{
}

void MainWindow::editingSceneRight(const Ray&)
{
}

void MainWindow::BoxMeshExample()
{
	Mesh boxMesh = Mesh(Box(3.0));

	vector<Color> cols;
	cols.resize(boxMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(boxMesh, cols, boxMesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::OperationExample(Operation operation)
{
	Mesh implicitMesh;

	switch (operation)
	{

	/* PRIMITIVES */
	case SPHERE:
		currentShape = new Sphere(Vector(0, 0, 0), 2);
		break;
	case CUBE:
		currentShape = new Cube(Vector(0, 0, 0), Vector(3, 3, 3));
		break;
	case CONE:
		currentShape = new Cone(0.2, 0.6, 5);
		break;
	case TORE:
		currentShape = new Tore(1, 3);
		break;
	case CAPSULE:
		currentShape = new Capsule(Vector(0, -2, 0), Vector(0, 2, 0), 1);
		break;
	case CYLINDER:
		currentShape = new Cylinder(Vector(0, 0, 0), 1.0, 4.0);
		break;

	/* UNARY OPERATORS */
	case TRANSLATION:
		currentShape = new Translation(new Sphere(Vector(0, 0, 0), 2), Vector(-1, -2, 1));
		break;
	case ROTATION:
		currentShape = new Rotation(new Tore(1, 3), Vector(0, 0, 60));
		break;
	case SCALE:
		currentShape = new Scale(new Sphere(Vector(0, 0, 0), 2), Vector(0.4, 0.4, 0.4));
		break;


	/* BINARY OPERATORS */
	case UNION:
		currentShape = new Union(new Sphere(Vector(-1, 0, 0), 2), new Sphere(Vector(1, 0, 0), 2));
		break;
	case INTERSECTION:
		currentShape = new Intersection(new Sphere(Vector(-1, 0, 0), 2), new Sphere(Vector(1, 0, 0), 2));
		break;
	case DIFF:
		currentShape = new Difference(new Sphere(Vector(-1, 0, 0), 2), new Sphere(Vector(1, 0, 0), 2));
		break;
	case UNION_SMOOTH:
		currentShape = new UnionSmooth(new Sphere(Vector(-1, 0, 0), 2), new Sphere(Vector(1, 0, 0), 2), 1);
		break;
	case INTERSECTION_SMOOTH:
		currentShape = new IntersectionSmooth(new Sphere(Vector(-1, 0, 0), 2), new Sphere(Vector(1, 0, 0), 2), 1);
		break;
	case DIFF_SMOOTH:
		currentShape = new DifferenceSmooth(new Sphere(Vector(-1, 0, 0), 2), new Sphere(Vector(1, 0, 0), 2), 1);
	break;

	default:
		cout << "Type de primitive invalide!" << endl;
		break;
	}

	sdf = Signed(currentShape);
	sdf.Polygonize(50, implicitMesh, Box(5.0));

	//implicitMesh.SaveObj("Results/objet.obj", "Objet");

	vector<Color> cols;
	cols.resize(implicitMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::toresComposition()
{
	Mesh implicitMesh;

	const double smoothFactor = 2;

	const double small_circle = 0.5;
	const double large_circle = 2;

	Rotation * rotation1 = new Rotation(new Tore(small_circle, large_circle), Vector(0, 0, 90));
	Translation * translation1 = new Translation(rotation1, Vector(2, 2, 0));
	UnionSmooth * unionSmooth1 = new UnionSmooth(new Tore(small_circle, large_circle), translation1, smoothFactor);

	Rotation* rotation2 = new Rotation(new Tore(small_circle, large_circle), Vector(0, 0, 90));
	Translation* translation2 = new Translation(rotation2, Vector(-2, 2, 0));
	UnionSmooth* unionSmooth2 = new UnionSmooth(unionSmooth1, translation2, smoothFactor);

	Translation* translation3 = new Translation(new Tore(small_circle, large_circle), Vector(0, 4, 0));
	UnionSmooth* unionSmooth3 = new UnionSmooth(unionSmooth2, translation3, smoothFactor);

	Rotation* rotation3 = new Rotation(new Tore(small_circle, large_circle), Vector(0, 90, 90));
	Translation* translation4 = new Translation(rotation3, Vector(0, 2, 2));
	UnionSmooth* unionSmooth4 = new UnionSmooth(unionSmooth3, translation4, smoothFactor);

	Rotation* rotation4 = new Rotation(new Tore(small_circle, large_circle), Vector(0, 90, 90));
	Translation* translation5 = new Translation(rotation4, Vector(0, 2, -2));
	UnionSmooth* unionSmooth5 = new UnionSmooth(unionSmooth4, translation5, smoothFactor);

	currentShape = unionSmooth5;
	sdf = Signed(currentShape);
	sdf.Polygonize(50, implicitMesh, Box(10.0));

	vector<Color> cols;
	cols.resize(implicitMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
	UpdateGeometry();
}

Node* lionShapes()
{
	const int factorSmooth = 1;

	// ================= T�te du lion =================
	Sphere* head = new Sphere(Vector(0.0, 0.0, 2.0), 2.5);
	Scale* scale_head = new Scale(head, Vector(1, 0.9, 1));


	// ================= Crini�re du lion =================
	Sphere* sphere1 = new Sphere(Vector(1.0, 0.0, 4.5), 0.7);
	Scale* scale1 = new Scale(sphere1, Vector(1.5, 0.8, 1.0));

	UnionSmooth* unionSmooth1 = new UnionSmooth(scale_head, scale1, factorSmooth);

	Sphere* sphere2 = new Sphere(Vector(-0.5, 0.0, 4.8), 0.8);
	Scale* scale2 = new Scale(sphere2, Vector(1.5, 0.8, 1.0));

	UnionSmooth* unionSmooth2 = new UnionSmooth(unionSmooth1, scale2, factorSmooth);

	Sphere* sphere3 = new Sphere(Vector(-1.6, 0.0, 1.6), 0.5);
	Scale* scale3 = new Scale(sphere3, Vector(1.5, 1, 2));

	UnionSmooth* unionSmooth3 = new UnionSmooth(unionSmooth2, scale3, factorSmooth);

	Sphere* sphere4 = new Sphere(Vector(1.8, 0.0, 1.3), 0.5);
	Scale* scale4 = new Scale(sphere4, Vector(1.5, 1, 2));

	UnionSmooth* unionSmooth4 = new UnionSmooth(unionSmooth3, scale4, factorSmooth);

	Sphere* sphere5 = new Sphere(Vector(1.5, 0.0, 0.2), 0.5);
	Scale* scale5 = new Scale(sphere5, Vector(1.5, 1, 2));

	UnionSmooth* unionSmooth5 = new UnionSmooth(unionSmooth4, scale5, factorSmooth);

	Sphere* sphere6 = new Sphere(Vector(-2.8, 0.0, 0.7), 0.7);
	Scale* scale6 = new Scale(sphere6, Vector(1, 1, 1.4));

	UnionSmooth* unionSmooth6 = new UnionSmooth(unionSmooth5, scale6, factorSmooth);

	Sphere* sphere7 = new Sphere(Vector(-0.7, 0.0, 0.1), 0.9);
	Scale* scale7 = new Scale(sphere7, Vector(1.6, 1, 1.5));

	UnionSmooth* unionSmooth7 = new UnionSmooth(unionSmooth6, scale7, factorSmooth);

	Sphere* sphere8 = new Sphere(Vector(0.4, 0.0, -0.6), 0.5);
	Scale* scale8 = new Scale(sphere8, Vector(2, 1, 1.5));

	UnionSmooth* unionSmooth8 = new UnionSmooth(unionSmooth7, scale8, factorSmooth);


	// ================= Yeux du lion =================
	Sphere* leftEye = new Sphere(Vector(-1.1, -3.4, 2.9), 0.4);
	Scale* scaleLeftEye = new Scale(leftEye, Vector(1.0, 0.6, 1.0));

	UnionSmooth* unionSmooth9 = new UnionSmooth(unionSmooth8, scaleLeftEye, factorSmooth);

	Sphere* rightEye = new Sphere(Vector(1.1, -3.4, 2.9), 0.4);
	Scale* scaleRightEye = new Scale(rightEye, Vector(1.0, 0.6, 1.0));

	UnionSmooth* unionSmooth10 = new UnionSmooth(unionSmooth9, scaleRightEye, factorSmooth);


	// ================= Museau du lion =================
	Sphere* muzzle = new Sphere(Vector(0, -1.8, 1.1), 1);
	Scale* scaleMuzzle = new Scale(muzzle, Vector(1.5, 0.7, 1.3));
	Rotation* rotMuzzle = new Rotation(scaleMuzzle, Vector(-20, 1, 1));

	UnionSmooth* unionSmooth11 = new UnionSmooth(unionSmooth10, rotMuzzle, factorSmooth);


	// ================= Moustache du lion =================
	Capsule* capsule1 = new Capsule(Vector(0.8, -3, 1), Vector(1.1, -3, 1), 0.15);
	Scale* scaleCapsule1 = new Scale(capsule1, Vector(2.6, 0.8, 1.2));
	Rotation* rotationCapsule1 = new Rotation(scaleCapsule1, Vector(1, 14, 1));

	UnionSmooth* unionSmooth12 = new UnionSmooth(unionSmooth11, rotationCapsule1, factorSmooth);

	Capsule* capsule2 = new Capsule(Vector(0.5, -3, 0.8), Vector(0.9, -3, 0.8), 0.15);
	Scale* scaleCapsule2 = new Scale(capsule2, Vector(2.6, 0.8, 1.2));
	Rotation* rotationCapsule2 = new Rotation(scaleCapsule2, Vector(1, -10, 1));

	UnionSmooth* unionSmooth13 = new UnionSmooth(unionSmooth12, rotationCapsule2, factorSmooth);

	Capsule* capsule3 = new Capsule(Vector(-1.1, -3, 1), Vector(-0.4, -3, 1), 0.15);
	Scale* scaleCapsule3 = new Scale(capsule3, Vector(2.6, 0.8, 1.2));
	Translation* translationCapsule3 = new Translation(scaleCapsule3, Vector(0.5, 0, -0.3));
	Rotation* rotationCapsule3 = new Rotation(translationCapsule3, Vector(0, 5, 0));

	UnionSmooth* unionSmooth14 = new UnionSmooth(unionSmooth13, rotationCapsule3, factorSmooth);

	Capsule* capsule4 = new Capsule(Vector(-1.1, -3, 1), Vector(-0.4, -3, 1), 0.15);
	Scale* scaleCapsule4 = new Scale(capsule4, Vector(2.6, 0.8, 1.2));
	Translation* translationCapsule4 = new Translation(scaleCapsule4, Vector(0, 0, 0.2));
	Rotation* rotationCapsule4 = new Rotation(translationCapsule4, Vector(0.5, -10, 0));

	UnionSmooth* unionSmooth15 = new UnionSmooth(unionSmooth14, rotationCapsule4, factorSmooth);


	// ================= Nez du lion =================
	Sphere* nose = new Sphere(Vector(0, -4.6, 3), 0.5);
	Scale* scaleNose = new Scale(nose, Vector(0.8, 0.6, 0.6));

	UnionSmooth* unionSmooth16 = new UnionSmooth(unionSmooth15, scaleNose, factorSmooth);


	// ================= Oreilles du lion =================
	Sphere* ear1 = new Sphere(Vector(2.5, -1.7, 1.8), 0.5);
	Scale* scaleEar1 = new Scale(ear1, Vector(1.4, 0.8, 1.3));
	Rotation* rotationEar1 = new Rotation(scaleEar1, Vector(0, 30, 0));

	UnionSmooth* unionSmooth17 = new UnionSmooth(unionSmooth16, rotationEar1, factorSmooth);

	Sphere* ear2 = new Sphere(Vector(-2.5, -1.7, 1.8), 0.5);
	Scale* scaleEar2 = new Scale(ear2, Vector(1.4, 0.8, 1.3));
	Rotation* rotationEar2 = new Rotation(scaleEar2, Vector(0, -30, 0));

	UnionSmooth* unionSmooth18 = new UnionSmooth(unionSmooth17, rotationEar2, factorSmooth);

	return new Translation(unionSmooth18, Vector(0, 0, -2));
}

void MainWindow::lionComposition()
{
	auto startTime = chrono::high_resolution_clock::now();

	Mesh implicitMesh;

	currentShape = lionShapes();

	sdf = Signed(currentShape);
	sdf.Polygonize(50, implicitMesh, Box(5.0));

	auto endTime = chrono::high_resolution_clock::now();
	auto elapsedMilliseconds = chrono::duration_cast<chrono::milliseconds>(endTime - startTime).count();

	//implicitMesh.SaveObj("Results/Lion.obj", "LionMesh");

	vector<Color> cols;
	cols.resize(implicitMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
	UpdateGeometry();

	cout << "Temps pour charger le lion : " << elapsedMilliseconds << " ms" << endl;
}

void MainWindow::measureBoxTime()
{
	vector<Ray> rays;

	random_device rd;
	mt19937 gen(rd());
	uniform_real_distribution<double> dis(-5.0, 5.0);

	long long total_without = 0;
	long long total_with = 0;

	unsigned int nb_rays = 400;

	// G�n�rer 10 Ray al�atoires et les ajouter au tableau
	for (unsigned int i = 0 ; i < nb_rays ; i++)
	{
		Vector origin(dis(gen), dis(gen), dis(gen)); // Origine al�atoire
		Vector direction(dis(gen), dis(gen), dis(gen)); // Direction al�atoire
		Ray ray(origin, direction); // Cr�ation du Ray avec l'origine et la direction al�atoires
		rays.push_back(ray); // Ajout du Ray au tableau
	}

	for (unsigned int i = 0 ; i < nb_rays ; i++ )
	{
		// Mesurer le temps d'ex�cution pour la fonction Intersect sans bo�tes englobantes.
		auto start_time_without_bounding_box = chrono::high_resolution_clock::now();
		double distance_without_bounding_box = 0.0;
		sdf.Intersect(rays.at(i), distance_without_bounding_box, false);
		auto end_time_without_bounding_box = chrono::high_resolution_clock::now();
		auto duration_without_bounding_box = chrono::duration_cast<chrono::microseconds>(end_time_without_bounding_box - start_time_without_bounding_box).count();

		total_without += duration_without_bounding_box;

		// Mesurer le temps d'ex�cution pour la fonction Intersect avec bo�tes englobantes.
		auto start_time_with_bounding_box = chrono::high_resolution_clock::now();
		double distance_with_bounding_box = 0.0;
		sdf.Intersect(rays.at(i), distance_with_bounding_box, true);
		auto end_time_with_bounding_box = chrono::high_resolution_clock::now();
		auto duration_with_bounding_box = chrono::duration_cast<chrono::microseconds>(end_time_with_bounding_box - start_time_with_bounding_box).count();
	
		total_with += duration_with_bounding_box;
	}
	cout << "Temps total sans boites englobantes : " << total_without << " ms" << endl;
	cout << "Temps total avec boites englobantes : " << total_with << " ms" << endl;
}

void MainWindow::editingErosion(const Ray& ray)
{
	double distance = 0.0;
	Mesh implicitMesh;

	const unsigned int typeErosion = 1;

	if (sdf.Intersect(ray, distance, true))
	{
		const Vector intersectionPoint = ray.Origin() + distance * ray.Direction();

		if (typeErosion == 1)
		{
			// �rosion incr�mentale
			unsigned int sphereSize = 0.7;
			currentShape = new DifferenceSmooth(currentShape, new Sphere(intersectionPoint, sphereSize), 1);
			sdf = Signed(currentShape);
		}
		else if (typeErosion == 2)
		{
			// �rosion par paquets
			constexpr double smallSphereSize = 0.05;
			constexpr double circleRadius = 0.7;
			constexpr unsigned int numSmallSpheres = 5;

			currentShape = new DifferenceSmooth(currentShape, new Sphere(intersectionPoint, smallSphereSize), 1);

			for (int i = 0; i < numSmallSpheres; i++)
			{
				double angle = (2 * M_PI * i) / numSmallSpheres;
				double xOffset = circleRadius * cos(angle);
				double zOffset = circleRadius * sin(angle);

				currentShape = new DifferenceSmooth(currentShape, new Sphere(intersectionPoint + Vector(xOffset, 0, zOffset), smallSphereSize), 1);
			}

			sdf = Signed(currentShape);
		}

		sdf.Polygonize(50, implicitMesh, Box(5.0));

		implicitMesh.SaveObj("Results/Erosion.obj", "Erosion");

		vector<Color> cols;
		cols.resize(implicitMesh.Vertexes());
		for (size_t i = 0; i < cols.size(); i++)
			cols[i] = Color(0.8, 0.8, 0.8);

		meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
		UpdateGeometry();
	}
}

void MainWindow::measureErosionTime()
{
	double distance = 0.0;
	Mesh implicitMesh;

	const unsigned int maxSpheres = 6; /// Multipe de 6
	constexpr double sphereSize = 0.5;

	currentShape = lionShapes();

	auto startTime1 = chrono::high_resolution_clock::now();

	for (unsigned int i = 0 ; i < maxSpheres; i++)
	{
		// �rosion incr�mentale
		currentShape = new DifferenceSmooth(currentShape, new Sphere(Vector(0, -2, 0), sphereSize), 1);
		sdf = Signed(currentShape);
	}

	sdf.Polygonize(50, implicitMesh, Box(5.0));

	vector<Color> cols;
	cols.resize(implicitMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
	UpdateGeometry();

	auto endTime1 = chrono::high_resolution_clock::now();
	auto elapsedMilliseconds1 = chrono::duration_cast<chrono::milliseconds>(endTime1 - startTime1).count();

	cout << "Temps pour eroser incrementalement " << maxSpheres << " spheres : " << elapsedMilliseconds1 << endl;

	currentShape = lionShapes();

	auto startTime2 = chrono::high_resolution_clock::now();

	for (unsigned int i = 0; i < maxSpheres/6; i++)
	{
		// �rosion par paquets
		constexpr double circleRadius = 0.7;
		constexpr unsigned int numSmallSpheres = 5;

		currentShape = new DifferenceSmooth(currentShape, new Sphere(Vector(0, -2, 0), sphereSize), 1);

		for (int i = 0; i < numSmallSpheres; i++)
		{
			double angle = (2 * M_PI * i) / numSmallSpheres;
			double xOffset = circleRadius * cos(angle);
			double zOffset = circleRadius * sin(angle);

			currentShape = new DifferenceSmooth(currentShape, new Sphere(Vector(0, -2, 0) + Vector(xOffset, 0, zOffset), sphereSize), 1);
		}

		sdf = Signed(currentShape);
	}

	sdf.Polygonize(50, implicitMesh, Box(5.0));

	cols.resize(implicitMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
	UpdateGeometry();

	auto endTime2 = chrono::high_resolution_clock::now();
	auto elapsedMilliseconds2 = chrono::duration_cast<chrono::milliseconds>(endTime2 - startTime2).count();

	cout << "Temps pour eroser par paquets " << maxSpheres << " spheres : " << elapsedMilliseconds2 << endl;
}


void MainWindow::displayBezier()
{
	/*
	const unsigned int n = 10;
	const unsigned int m = 10;

	vector<Vector> pc;
	srand(time(NULL));
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			int y = j && j < m - 1 ? rand() % n : 0;
			pc.emplace_back(j, y, i);
			cout << "j: " << j << ", y: " << y << ", i: " << i << endl;
		}
	}

	BezierCurve* box = BezierCurve::createPatch(pc, m, n, 200);
	Mesh meshBezier = box->Polygonize();

	currentShape = box;
	sdf = Signed(currentShape);

	vector<Color> cols;
	cols.resize(meshBezier.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(meshBezier, cols, meshBezier.VertexIndexes());
	UpdateGeometry();*/
}

void MainWindow::UpdateGeometry()
{
	meshWidget->ClearAll();
	meshWidget->AddMesh("BoxMesh", meshColor);

    uiw->lineEdit->setText(QString::number(meshColor.Vertexes()));
    uiw->lineEdit_2->setText(QString::number(meshColor.Triangles()));

	UpdateMaterial();
}

void MainWindow::UpdateMaterial()
{
    meshWidget->UseWireframeGlobal(uiw->wireframe->isChecked());

    if (uiw->radioShadingButton_1->isChecked())
		meshWidget->SetMaterialGlobal(MeshMaterial::Normal);
	else
		meshWidget->SetMaterialGlobal(MeshMaterial::Color);
}

void MainWindow::ResetCamera()
{
	meshWidget->SetCamera(Camera(Vector(-10.0), Vector(0.0)));
}
