#include "signed.h"
#include <iostream>
#include <ui_interface.h>
#include "qte.h"

using namespace std;

const Box box = Box(3.0);

bool Signed::Intersect(const Ray& ray, double& distance, const bool& boundingBox) const
{
    // Param�tres de l'algorithme de Sphere Tracing.
    constexpr double epsilon = 0.05; // Tol�rance pour la d�tection de collision.
    constexpr int maxIterations = 100; // Nombre maximal d'it�rations.

    constexpr double maxBoundingDistance = 20.0;

    double t = 0.0; // Param�tre de distance le long du rayon initialis� � 0.

    // V�rifie si le point initial est � l'int�rieur de la bo�te englobante.
    if (boundingBox && !box.Inside(ray.Origin()))
    {
        // Avance le long du rayon jusqu'� ce qu'il p�n�tre dans la bo�te.
        while (t < maxBoundingDistance)
        {
            const Vector currentPos = ray.Origin() + t * ray.Direction(); // Position actuelle le long du rayon.

            // V�rifie si le point est � l'int�rieur de la bo�te englobante.
            if (box.Inside(currentPos))
            {
                break; // Sort de la boucle si le point est � l'int�rieur de la bo�te.
            }

            t += epsilon; // Avance le long du rayon par incr�ment pour �viter une boucle infinie.
        }
    }

    for (unsigned int i = 0; i < maxIterations; ++i)
    {
        const Vector currentPos = ray.Origin() + t * ray.Direction(); // Position actuelle le long du rayon.

        // V�rifie si le point est � l'int�rieur de la bo�te englobante.
        if (boundingBox && !box.Inside(currentPos))
        {
            return false; // Si le point n'est pas � l'int�rieur de la bo�te, pas d'intersection.
        }

        // Calcule la valeur de la surface implicite.
        const double currentValue = Value(currentPos);

        // V�rifie si le point est suffisamment proche de la surface implicite.
        if (fabs(currentValue) < epsilon)
        {
            distance = t;
            return true; // Intersection trouv�e.
        }

        // Avance le long du rayon en fonction de la valeur de la surface implicite.
        t += currentValue;

        // Si t devient n�gatif, le rayon est sorti de l'objet.
        if (t < 0.0)
            return false;
    }

    return false; // Nombre maximal d'it�rations sans intersection => pas d'intersection.
}

