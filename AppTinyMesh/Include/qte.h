#ifndef __Qte__
#define __Qte__

#include <QtWidgets/qmainwindow.h>
#include "realtime.h"
#include "meshcolor.h"
#include "../signed.h"
#include "../node.h"

enum Operation
{
    SPHERE = 0,
    CUBE = 1,
    CONE = 2,
    TORE = 3,
    CAPSULE = 4,
    CYLINDER = 5,
    TRANSLATION = 6,
    ROTATION = 7,
    SCALE = 8,
    UNION = 9,
    INTERSECTION = 10,
    DIFF = 11,
    UNION_SMOOTH = 12,
    INTERSECTION_SMOOTH = 13,
    DIFF_SMOOTH = 14
};


QT_BEGIN_NAMESPACE
	namespace Ui { class Assets; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT
private:
  Ui::Assets* uiw;           //!< Interface

  MeshWidget* meshWidget;   //!< Viewer
  MeshColor meshColor;		//!< Mesh.

public:
  MainWindow();
  ~MainWindow();
  void CreateActions();
  void UpdateGeometry();

  Signed sdf;
  Node* currentShape;

public slots:
  void editingSceneLeft(const Ray&);
  void editingSceneRight(const Ray&);
  void BoxMeshExample();
  void OperationExample(Operation operation);
  void toresComposition();
  void lionComposition();
  void editingErosion(const Ray&);
  void measureBoxTime();
  void measureErosionTime();
  void displayBezier();
  void ResetCamera();
  void UpdateMaterial();
};

#endif
